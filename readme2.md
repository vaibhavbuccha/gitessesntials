##### Branching
---

- [x] git branch
1. create branch and switch to that branch
```bash
git checkout -b <branchName>
```

2. create new branch
```bash
git branch <branchName>
```
