## Git Essentials
---
- [x] git clone

> git clone create a copy of your remote repo in your local machine.

```bash
git clone <RemoteUrl>
```
- [x] git status

> check status of our your current reposity and show which files are changed.

```bash
git status
```
- [x] git add 

> git add is use for add file or stage file.

```bash
git add <filename/path> # ( . ) for stage all files
```
---
- [x] git pull

> 1. git pull is used to download and fetch the content form the remote repo. 
> 2. git pull is basically conbination of 2 commands.

>		* git fetch.
>       * git merge.

*Workflow*

> git pull first execute git fetch it download the repo in seprate tree. after download completion git merge is work it merge our download code with local head.

```bash
git pull origin <originName>
```
> verbose flag output during a pull which displays the content being downloaded and the merge details
```bash
git pull --verbose
```
---

